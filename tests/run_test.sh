#!/bin/bash
set -e

NRCPUS=$(getconf _NPROCESSORS_ONLN)
patchfile="$PWD/libpmemobj-test-installed-libs.patch"

cd source

patch -p1 < $patchfile

mkdir build
cd build
cmake .. -DTESTS_USE_VALGRIND=OFF -DTESTS_LONG=OFF -DTESTS_USE_FORCED_PMEM=ON
make -j $NRCPUS

ctest --output-on-failure
